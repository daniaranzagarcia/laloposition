class PostParams {
  var params;

  PostParams({this.params});

  factory PostParams.fromJson(Map<String, dynamic> json) {
    return new PostParams(
      params: json['data'],
    );
  }
}
