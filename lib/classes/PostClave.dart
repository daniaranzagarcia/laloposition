class PostClave {
  final String clave;

  PostClave({this.clave});

  factory PostClave.fromJson(Map<String, dynamic> json) {
    return new PostClave(
      clave: json['data']['claveadmin'],
    );
  }
}
