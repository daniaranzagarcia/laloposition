import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:laloposition/main.dart';
import 'package:path_provider/path_provider.dart';
import 'globals.dart';
import 'package:http/http.dart' as http;
import 'package:convert/convert.dart';
import 'globals.dart' as globals;

//Toast's
void showMessage(String msg, gravity) {
  Fluttertoast.showToast(
      msg: msg,
      fontSize: 12,
      backgroundColor: Colors.black87,
      textColor: Colors.white,
      gravity: gravity,
      toastLength: Toast.LENGTH_LONG,
      timeInSecForIos: 3);
}

class SiteDialogOverlay extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SiteDialogOverlayState();
}

class SiteDialogOverlayState extends State<SiteDialogOverlay>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  final myController = TextEditingController();
  final myController2 = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    myController2.dispose();
    super.dispose();
  }

  @override
  void initState() {
    //L2a0L1o6.pwd*
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  void _exitDialog() {
    Navigator.of(context).pop();
  }

  void _save() async {
    final prefs = await SharedPreferences.getInstance();

    prefs.setString('settings', myController.text + ";" + myController2.text);
    showMessage("Configuración guardada correctamente", ToastGravity.BOTTOM);
    terminal = "" + myController2.text;
    servicio = "" + myController.text;

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyApp()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Dialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
      child: new ListView(
        padding: EdgeInsets.all(20),
        shrinkWrap: true,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Configuración",
                textAlign: TextAlign.center,
                style: TextStyle(fontFamily: 'Montserrat', fontSize: 25.0),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 35),
              ),
              TextField(
                  controller: myController,
                  textAlign: TextAlign.center,
                  textAlignVertical: TextAlignVertical.center,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 15.0),
                      hintText: "Ingresa la Terminal",
                      prefixIcon: Icon(Icons.alternate_email),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0)))),
              Padding(
                padding: EdgeInsets.only(bottom: 15),
              ),
              TextField(
                  controller: myController2,
                  textAlign: TextAlign.center,
                  textAlignVertical: TextAlignVertical.center,
                  decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 15.0),
                      hintText: "Ingrese el Servicio",
                      prefixIcon: Icon(Icons.info),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15.0)))),
              Padding(
                padding: EdgeInsets.only(bottom: 15),
              ),
              Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(15.0),
                color: Colors.blueAccent,
                child: MaterialButton(
                  minWidth: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () => (myController.text.isNotEmpty &&
                          myController2.text.isNotEmpty)
                      ? _save()
                      : showMessage(
                          "Debe llenar los campos", ToastGravity.BOTTOM),
                  child: Text("Aplicar",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Montserrat',
                          fontSize: 16.0,
                          color: Colors.white)),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

//Add Dialog
class AddDialogOverlay extends StatefulWidget {
  final String ref;

  const AddDialogOverlay({Key key, this.ref}) : super(key: key);

  @override
  State<StatefulWidget> createState() => AddDialogOverlayState(ref);
}

class AddDialogOverlayState extends State<AddDialogOverlay>
    with SingleTickerProviderStateMixin {
  final String ref;
  AnimationController controller;
  Animation<double> scaleAnimation;
  final myController = TextEditingController();
  bool success = false;

  AddDialogOverlayState(this.ref);

  //File Methods

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get localFile async {
    final path = await _localPath;
    return File('$path/LALOhys' + ref + '.txt');
  }

  Future<File> writeContent(String data) async {
    final file = await localFile;
    // Write the file

    return file.writeAsString(data);
  }

  Future<String> readContent() async {
    try {
      final file = await localFile;
      // Read the file
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      print(e.toString());
      return 'Error:';
    }
  }

  //--

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _getLocation();
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  void _save() async {
    if (!isLoading) {
      await writeContent("$terminal;" +
          ref +
          ";" +
          servicio +
          ";" +
          _position.latitude.toString() +
          ";" +
          _position.longitude.toString());
      print("añadido");
      callWebService();

      Navigator.pushNamedAndRemoveUntil(context, "/main", (r) => false);

//      Navigator.popUntil(context, ModalRoute.withName('/main'));
//      Navigator.push(
//        context,
//        MaterialPageRoute(builder: (context) => MyApp()),
//      );
    } else {
      showMessage("La ubicacion aun no esta lista", ToastGravity.BOTTOM);
    }
  }

  Future<http.Response> callWebService() async {
    final Map<String, dynamic> params = {
      "apikey": '' + globals.params['cliente'],
      "referencia": '' + ref,
      "georef":
          _position.latitude.toString() + ";" + _position.longitude.toString(),
      "terminal": '' + terminal,
    };
    print(json.encode(params));
    final post = await http.post(
      "https://isysingenieria.mobi/cloudhosting/IsysEncuestas/portal/laloPositionReferencia.php", // change with your API
      body: json.encode(params),
      //headers: paramDic
    );

    if (post.statusCode == 200) {
      setState(() => success = true);
    }
    print(post.body);

    return post;
  }

  void _exitDialog() {
    Navigator.of(context).pop();
  }

  Position _position;
  var isLoading = true;
  _getLocation() async {
    _position = await Geolocator()
        .getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      isLoading = false;
      _save();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Dialog(
      shape:
          RoundedRectangleBorder(borderRadius: new BorderRadius.circular(20.0)),
      child: new Container(
        height: 200,
        margin: EdgeInsets.only(top: 30, right: 30, left: 30, bottom: 15),
        child: Column(
          children: <Widget>[
            new Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  isLoading
                      ? new Column(
                          children: <Widget>[
                            Text("Obteniendo Ubicacion"),
                            new CircularProgressIndicator()
                          ],
                        )
                      : success
                          ? new Icon(Icons.done)
                          : new Column(
                              children: <Widget>[
                                Text("Enviando Datos"),
                                new CircularProgressIndicator()
                              ],
                            ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
