import 'dart:async';
import 'dart:io' as io;
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:laloposition/routes/simple_map.dart';
import 'package:path_provider/path_provider.dart';

import 'package:flutter/cupertino.dart';
import 'classes/Dialogs.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'classes/PostClave.dart';
import 'classes/PostParams.dart';
import 'classes/globals.dart';
import 'package:http/http.dart' as http;
import 'routes/map.dart';
import 'package:convert/convert.dart';

void main() => runApp(MyApp());

//Post Methods

Future<PostClave> _fetchDataClave() async {
  final response = await http
      .get("https://isysingenieria.mobi/cloudhosting/laloPosition/claveadmin");
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    return PostClave.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

Future<PostParams> _fetchDataParams() async {
  final response = await http.get(
      "https://isysingenieria.mobi/cloudhosting/laloPosition/parametros/apikey/FundDignitas@encuesta.1");
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    return PostParams.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

//--

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'LaLoPosition',
      theme: ThemeData(
          primarySwatch: Colors.green, accentColor: Colors.greenAccent),
      debugShowMaterialGrid: false,
      debugShowCheckedModeBanner: false,
      initialRoute: '/main',
      routes: {
        '/main': (context) => MyHomePage(title: 'LaLoPosition'),
        '/add': (context) => AddDialogOverlay(),
        '/conf': (context) => SiteDialogOverlay(),
        '/map': (context) => MapSample(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<PostClave> postClave;
  Future<PostParams> postParams;

  final myController = TextEditingController();
  bool firstLoad = true;
  bool loadServices = true;
  bool exist = false;
  bool hysLoading = true;
  bool cond = true;
  String directory;
  List file = new List();
  List files = new List();
  List names = new List();
  List datas = new List();
  String id = '';

  Future<List<String>> history;

  Future<void> _requestPermissions() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([
      PermissionGroup.locationAlways,
      PermissionGroup.location,
      PermissionGroup.locationWhenInUse,
      PermissionGroup.access_media_location,
      PermissionGroup.storage,
    ]);
  }

  Future<void> _checkExist() async {
    final prefs = await SharedPreferences.getInstance();
    final settings = prefs.getString('settings') ?? "";
    if (settings.isNotEmpty) {
      servicio = settings.split(';')[1];
      print(servicio);
      setState(() {
        exist = true;
      });
    } else {
      await _openDialog();
    }
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  Future<void> _openDialog() async {
    final passController = TextEditingController();

    void _save(BuildContext buildContext) async {
      final prefs = await SharedPreferences.getInstance();

      List deClave = hex.decode('$clave');
      String pass = '';
      for (int i in deClave) {
        pass += '' + String.fromCharCode(i);
      }
      print('\n$pass\n');
      if (passController.text == pass) {
        prefs.setString('clave', passController.text);
        clave = "" + passController.text;
        Navigator.of(buildContext).pop();
        showDialog(
          barrierDismissible: true,
          context: context,
          builder: (_) => SiteDialogOverlay(),
        );
      } else {
        showMessage("Clave invalida", ToastGravity.BOTTOM);
      }
    }

//    void _check(BuildContext buildContext) {
//      List deClave = hex.decode('$clave');
//      String pass = '';
//      for (int i in deClave) {
//        pass += '' + String.fromCharCode(i);
//      }
//      print('\n$pass\n');
//      if (passController.text == pass) {
//        Navigator.of(buildContext).pop();
//        showDialog(
//          barrierDismissible: true,
//          context: context,
//          builder: (_) => SiteDialogOverlay(),
//        );
//      } else {
//        showMessage("Referencia invalida", ToastGravity.BOTTOM);
//      }
//    }

    if (exist) {
      showDialog(
        barrierDismissible: true,
        context: context,
        builder: (BuildContext buildContext) {
          return new Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(20.0)),
              child: new ListView(
                  padding: EdgeInsets.all(20),
                  shrinkWrap: true,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Text(
                          "Solicitud de Configuración",
                          style: TextStyle(
                              fontFamily: 'Montserrat', fontSize: 20.0),
                          textAlign: TextAlign.start,
                        ),
                        new Padding(padding: EdgeInsets.only(bottom: 20)),
                        new Text(
                          "Ingrese la clave de administración:",
                          style: TextStyle(
                              fontFamily: 'Montserrat', fontSize: 16.0),
                          textAlign: TextAlign.start,
                        ),
                        new Padding(padding: EdgeInsets.only(bottom: 10)),
                        TextField(
                          maxLines: null,
                          minLines: null,
                          controller: passController,
                          textAlign: TextAlign.center,
                          textAlignVertical: TextAlignVertical.center,
                          decoration: InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(10.0, 15.0, 20.0, 15.0),
                              hintText: "********",
                              prefixIcon: Icon(Icons.lock),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15.0))),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 15),
                        ),
                        Material(
                          elevation: 5.0,
                          borderRadius: BorderRadius.circular(15.0),
                          color: Colors.blueAccent,
                          child: MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: () => _save(buildContext),
                            child: Text("Continuar",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontSize: 16.0,
                                    color: Colors.white)),
                          ),
                        ),
                      ],
                    )
                  ]));
        },
      );
    } else {
      showDialog(
        barrierDismissible: true,
        context: context,
        builder: (_) => SiteDialogOverlay(),
      );
    }
  }

  Future<void> _openAddDialog() async {
    int min = (params['longitud_min_referencia']);
    int max = (params['longitud_max_referencia']);
    if (myController.text.length <= max && myController.text.length >= min) {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (_) => AddDialogOverlay(
          ref: myController.text,
        ),
      );
    } else {
      showMessage("Referencia Invalida", ToastGravity.BOTTOM);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    _requestPermissions();
    postClave = _fetchDataClave();
    postParams = _fetchDataParams();
    _listFiles();
    _checkExist();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: <Widget>[
            PopupMenuButton(
              itemBuilder: (_) => <PopupMenuItem<String>>[
                new PopupMenuItem(
                    child: ListTile(
                  title: Text(
                    "Configurar",
                    style: TextStyle(fontSize: 15),
                  ),
                  onTap: () => _openDialog(),
                  leading: Icon(
                    Icons.settings,
                    color: Colors.grey,
                  ),
                )),
                new PopupMenuItem(
                    child: ListTile(
                  title: Text(
                    "Limpiar Historial",
                    style: TextStyle(fontSize: 15),
                  ),
                  onTap: () {
                    try {
                      final dir = io.Directory(directory + '/');
                      dir.deleteSync(recursive: true);
                      Navigator.pushNamedAndRemoveUntil(
                          context, "/main", (r) => false);
                    } catch (e) {}
                  },
                  leading: Icon(
                    Icons.delete,
                    color: Colors.grey,
                  ),
                )),
                new PopupMenuItem(
                    child: ListTile(
                  onTap: () {
                    Navigator.pushNamed(context, "/map");
                  },
                  title: Text(
                    "Verificar Ubicación",
                    style: TextStyle(fontSize: 15),
                  ),
                  leading: Icon(
                    Icons.map,
                    color: Colors.grey,
                  ),
                )),
              ],
            ),
          ],
        ),
        body: new FutureBuilder<PostClave>(
          future: postClave,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return new FutureBuilder<PostParams>(
                future: postParams,
                builder: (context2, snapshot2) {
                  if (snapshot2.hasData) {
                    //Save Data in Globals Variables
                    clave = snapshot.data.clave;
                    params = snapshot2.data.params;

                    var now = new DateTime.now();
                    DateTime vig = DateTime.parse(params['vigencia']);
                    print(now.toIso8601String());

                    if (vig.isBefore(now) && firstLoad) {
                      showMessage(
                          "La vigencia del servicio ha vencido, consulte con soporte técnico para más información",
                          ToastGravity.BOTTOM);
                    }

                    if (SchedulerBinding.instance.schedulerPhase ==
                        SchedulerPhase.persistentCallbacks) {
                      if (firstLoad) {
                        SchedulerBinding.instance.addPostFrameCallback(
                            (_) => setLoadedServices(context));
                      }
                    }
                    return new Container(
                        padding: EdgeInsets.only(bottom: 80),
                        child: Center(
                            child: exist
                                ? hysLoading
                                    ? Center(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.all(50),
                                            ),
                                            Text("Cargando..."),
                                            Padding(
                                              padding: EdgeInsets.all(15),
                                            ),
                                            CircularProgressIndicator()
                                          ],
                                        ),
                                      )
                                    : Container(
                                        padding: EdgeInsets.all(8),
                                        child: Column(
                                          children: <Widget>[
                                            // your Content if there
                                            Expanded(
                                              child: ListView.builder(
                                                  padding: EdgeInsets.only(
                                                      top: 16.0,
                                                      bottom: 16.0,
                                                      right: 5,
                                                      left: 8),
                                                  itemCount: datas.length,
                                                  itemBuilder:
                                                      (BuildContext context,
                                                          int index) {
                                                    var dataArr = datas[index]
                                                        .toString()
                                                        .split(";");

                                                    return Card(
                                                      elevation: 3,
                                                      child: ListTile(
                                                        title: Text(dataArr[1]),
                                                        subtitle: Text(
                                                            dataArr[3] +
                                                                "~" +
                                                                dataArr[4]),
                                                        onTap: () {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (context) =>
                                                                          SimpleMapSample(
                                                                            lat:
                                                                                double.parse(dataArr[3]),
                                                                            lng:
                                                                                double.parse(dataArr[4]),
                                                                            ref:
                                                                                dataArr[1],
                                                                          )));
                                                        },
                                                        leading: Icon(
                                                          Icons.map,
                                                          color:
                                                              Colors.blueAccent,
                                                        ),
                                                      ),
                                                    );
                                                  }),
                                            ),
                                          ],
                                        ))
                                : new Center(
                                    child: new CircularProgressIndicator(
                                      backgroundColor: Colors.blueAccent,
                                    ),
                                  )));
                  } else {
                    return new Center(
                      child: new CircularProgressIndicator(
                        backgroundColor: Colors.blueAccent,
                      ),
                    );
                  }
                },
              );
            } else {
              return new Center(
                child: new CircularProgressIndicator(
                  backgroundColor: Colors.blueAccent,
                ),
              );
            }
          },
        ),
        floatingActionButton: exist
            ? !loadServices
                ? Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        SizedBox(
                          width: MediaQuery.of(context).size.width - 130,
                          child: TextField(
                            controller: myController,
                            textAlign: TextAlign.start,
                            textAlignVertical: TextAlignVertical.center,
                            decoration: InputDecoration(hintText: 'Referencia'),
                            keyboardType: TextInputType.number,
                            inputFormatters: (params['tipo_dato_captura'] ==
                                    'numerico')
                                ? <TextInputFormatter>[
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ]
                                : <TextInputFormatter>[],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 10),
                        ),
                        FloatingActionButton(
                          backgroundColor: Colors.blueAccent,
                          heroTag: "btn1",
                          onPressed: _openAddDialog,
                          child: Icon(
                            Icons.send,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  )
                : new FloatingActionButton(
                    backgroundColor: Colors.green,
                    onPressed: null,
                    child: Padding(
                      child: new CircularProgressIndicator(
                        strokeWidth: 2,
                        backgroundColor: Colors.white,
                      ),
                      padding: EdgeInsets.all(0),
                    ),
                  )
            : new FloatingActionButton(
                backgroundColor: Colors.blueAccent,
                onPressed: _openDialog,
                child: Padding(
                  child: Icon(
                    Icons.settings,
                    color: Colors.white,
                  ),
                  padding: EdgeInsets.all(0),
                ),
              )
        // This trailing comma makes auto-formatting nicer for build methods.
        );
  }

  Future<void> _listFiles() async {
    if (cond) {
      cond = false;
      directory = (await getApplicationDocumentsDirectory()).path;
      print("dir:" + directory);
      file = io.Directory("$directory/")
          .listSync(); //use your folder name instead of resume.

      for (var f in file) {
        f.toString().contains("LALOhys") ? files.add(f) : null;
      }
      await _nameFiles();
    }
  }

  Future<void> _nameFiles() async {
    for (var f in files) {
      var arr = f.toString().split("File: '$directory");
      var name = arr[1].split("'");
      names.add(name[0]);
      await _dataFiles(name[0]);
      //print(name[0]);
    }

    setState(() {
      hysLoading = false;
    });
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<io.File> get _localFile async {
    final path = await _localPath;
    //print("dir2:$path$id");
    return io.File('$path' + id);
  }

  Future<String> readContent() async {
    try {
      final file = await _localFile;
      // Read the file
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      print(e.toString());
      return 'Error:';
    }
  }

  Future<void> _dataFiles(String n) async {
    id = n;
    await readContent().then((String value) {
      datas.add(value);
      //print(value);
    });
  }

  void setLoadedServices(context) {
    setState(() => loadServices = false);
    print("loaded");
    firstLoad = false;
  }
}
