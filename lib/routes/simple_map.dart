import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:laloposition/classes/Dialogs.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:laloposition/classes/globals.dart';

class SimpleMapSample extends StatefulWidget {
  final double lat;
  final double lng;
  final String ref;

  const SimpleMapSample({Key key, this.lat, this.lng, this.ref})
      : super(key: key);
  @override
  State<SimpleMapSample> createState() => SimpleMapSampleState(lat, lng, ref);
}

class SimpleMapSampleState extends State<SimpleMapSample> {
  final double lat;
  final double lng;
  final String ref;
  SimpleMapSampleState(this.lat, this.lng, this.ref);
  Completer<GoogleMapController> _controller = Completer();
  bool _GPS = false;
  Position _position;
  var isLoading = true;

  Set<Marker> markers = Set();
  Marker site;

  void addMarker() {
    double c = BitmapDescriptor.hueRed;
    for (var i = 0; i <= colors.length; i++) {
      if (colors[i] == params['color_marcador']) {
        c = hues[i];
        break;
      }
    }
    site = new Marker(
        icon: BitmapDescriptor.defaultMarkerWithHue(c),
        infoWindow: new InfoWindow(
          title: params['texto_marcador_mapa'] + " $ref",
        ),
        onTap: () {
          showMessage(
              params['texto_marcador_mapa'] + " $ref", ToastGravity.BOTTOM);
        },
        markerId: MarkerId('$ref'),
        position: LatLng(lat, lng));

    markers.addAll([site]);
  }

  _getLocation() async {
    _position = await Geolocator()
        .getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      isLoading = false;
      addMarker();
    });
  }

  @override
  Widget build(BuildContext context) {
    _checkGPS();
    return new Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            tooltip: "Ir a mi ubicación",
            onPressed: _goToMyLocation,
            icon: Icon(Icons.location_on),
          )
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "LaLoPosition\nMapa",
          style: TextStyle(fontSize: 16),
          textAlign: TextAlign.start,
        ),
        centerTitle: false,
      ),
      body: _GPS
          ? (isLoading
              ? Center(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(50),
                      ),
                      Text("Cargando..."),
                      Padding(
                        padding: EdgeInsets.all(15),
                      ),
                      CircularProgressIndicator()
                    ],
                  ),
                )
              : GoogleMap(
                  markers: markers,
                  mapType: MapType.normal,
                  myLocationEnabled: true,
                  compassEnabled: true,
                  buildingsEnabled: true,
                  indoorViewEnabled: true,
                  mapToolbarEnabled: true,
                  myLocationButtonEnabled: false,
                  rotateGesturesEnabled: true,
                  trafficEnabled: false,
                  zoomGesturesEnabled: true,
                  scrollGesturesEnabled: true,
                  tiltGesturesEnabled: true,
                  initialCameraPosition: CameraPosition(
                      target: LatLng(_position.latitude, _position.longitude),
                      zoom: 19.151926040649414),
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                ))
          : Text("Activa la ubicación!"),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.blueAccent,
        foregroundColor: Colors.white,
        onPressed: _goToSiteLocation,
        label: FlatButton.icon(
          icon: Icon(
            Icons.subdirectory_arrow_right,
            color: Colors.white,
          ),
          label: Text(
            "Ir al sitio!",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }

  Future<void> _goToMyLocation() async {
    _getLocation();
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
        target: LatLng(_position.latitude, _position.longitude), zoom: 20)));
  }

  Future<void> _goToSiteLocation() async {
    if (lat != null && lng != null) {
      final GoogleMapController controller = await _controller.future;
      controller.animateCamera(CameraUpdate.newCameraPosition(
          new CameraPosition(target: LatLng(lat, lng), zoom: 18)));
    }
  }

  _checkGPS() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([
      PermissionGroup.locationAlways,
      PermissionGroup.location,
      PermissionGroup.locationWhenInUse,
      PermissionGroup.access_media_location,
      PermissionGroup.storage,
      PermissionGroup.photos
    ]);

    ServiceStatus serviceStatus =
        await PermissionHandler().checkServiceStatus(PermissionGroup.location);
    bool enabled = (serviceStatus == ServiceStatus.enabled);

    if (enabled) {
      setState(() {
        this._GPS = true;
      });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext buildContext) {
            return new AlertDialog(
              title: Text("Debe Activar la Ubicación"),
              content: Text(
                "Active los servicios de ubicación para poder desplegar el mapa",
                textAlign: TextAlign.justify,
              ),
              actions: <Widget>[
                new FlatButton(
                    onPressed: () => Navigator.pushNamed(context, '/main'),
                    child: new Text("Aceptar"))
              ],
            );
          });
    }
  }

  @override
  void initState() {
    super.initState();
    _checkGPS();
    _getLocation();
  }
}
