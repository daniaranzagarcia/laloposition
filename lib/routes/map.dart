import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:laloposition/classes/globals.dart';

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  Completer<GoogleMapController> _controller = Completer();
  bool _GPS = false;
  Position _position;
  final latController = TextEditingController();
  final lngController = TextEditingController();
  var isLoading = true;
  _getLocation() async {
    _position = await Geolocator()
        .getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    _checkGPS();
    return new Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            tooltip: "Ir a mi ubicación",
            onPressed: _goToMyLocation,
            icon: Icon(Icons.location_on),
          )
        ],
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "LaLoPosition\nMapa",
          style: TextStyle(fontSize: 16),
          textAlign: TextAlign.start,
        ),
        centerTitle: false,
      ),
      body: _GPS
          ? (isLoading
              ? Center(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(50),
                      ),
                      Text("Cargando..."),
                      Padding(
                        padding: EdgeInsets.all(15),
                      ),
                      CircularProgressIndicator()
                    ],
                  ),
                )
              : GoogleMap(
                  mapType: MapType.normal,
                  myLocationEnabled: true,
                  compassEnabled: true,
                  buildingsEnabled: true,
                  indoorViewEnabled: true,
                  mapToolbarEnabled: true,
                  myLocationButtonEnabled: false,
                  rotateGesturesEnabled: true,
                  trafficEnabled: false,
                  zoomGesturesEnabled: true,
                  scrollGesturesEnabled: true,
                  tiltGesturesEnabled: true,
                  initialCameraPosition: CameraPosition(
                      target: LatLng(_position.latitude, _position.longitude),
                      zoom: 19.151926040649414),
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                ))
          : Text("Activa la ubicación!"),
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SizedBox(
              width: (MediaQuery.of(context).size.width - 125) / 2,
              child: TextField(
                controller: latController,
                textAlign: TextAlign.start,
                textAlignVertical: TextAlignVertical.center,
                decoration: InputDecoration(hintText: 'Latitud'),
                keyboardType: TextInputType.numberWithOptions(
                    signed: true, decimal: true),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 5),
            ),
            SizedBox(
              width: (MediaQuery.of(context).size.width - 125) / 2,
              child: TextField(
                controller: lngController,
                textAlign: TextAlign.start,
                textAlignVertical: TextAlignVertical.center,
                decoration: InputDecoration(hintText: 'Longitud'),
                keyboardType: TextInputType.numberWithOptions(
                    signed: true, decimal: true),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10),
            ),
            FloatingActionButton(
              backgroundColor: Colors.blueAccent,
              heroTag: "btn1",
              onPressed: _goToSiteLocation,
              child: Icon(
                Icons.search,
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _goToMyLocation() async {
    _getLocation();
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
        target: LatLng(_position.latitude, _position.longitude), zoom: 20)));
  }

  Future<void> _goToSiteLocation() async {
    if (latController.text != null && lngController.text != null) {
      if (latController.text.isNotEmpty && lngController.text.isNotEmpty) {
        final GoogleMapController controller = await _controller.future;
        controller.animateCamera(CameraUpdate.newCameraPosition(
            new CameraPosition(
                target: LatLng(double.parse(latController.text),
                    double.parse(lngController.text)),
                zoom: 18)));
      }
    }
  }

  _checkGPS() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([
      PermissionGroup.locationAlways,
      PermissionGroup.location,
      PermissionGroup.locationWhenInUse,
      PermissionGroup.access_media_location,
      PermissionGroup.storage,
    ]);

    ServiceStatus serviceStatus =
        await PermissionHandler().checkServiceStatus(PermissionGroup.location);
    bool enabled = (serviceStatus == ServiceStatus.enabled);

    if (enabled) {
      setState(() {
        this._GPS = true;
      });
    } else {
      showDialog(
          context: context,
          builder: (BuildContext buildContext) {
            return new AlertDialog(
              title: Text("Debe Activar la Ubicación"),
              content: Text(
                "Active los servicios de ubicación para poder desplegar el mapa",
                textAlign: TextAlign.justify,
              ),
              actions: <Widget>[
                new FlatButton(
                    onPressed: () => Navigator.pushNamed(context, '/main'),
                    child: new Text("Aceptar"))
              ],
            );
          });
    }
  }

  @override
  void initState() {
    super.initState();
    _checkGPS();
    _getLocation();
  }
}
